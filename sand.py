from PIL import Image
import numpy as np

SAND = 120
EMPTY = 225
fp_in = "image3.png"
fp_out = "image3.gif"
im = np.asarray(Image.open(fp_in).convert(mode="P")).copy()
imgs = []


def handle_rect(im, row, col):
    if im[row, col] == SAND and im[row + 1, col] == EMPTY:
        im[row, col] = EMPTY
        im[row + 1, col] = SAND


def handle_square(im, row, col, dir):
    if im[row, col] == SAND \
            and im[row + 1, col] == SAND \
            and im[row + 1, col + dir] == EMPTY:
        im[row + 1, col + dir] = SAND
        im[row + 1, col] = EMPTY


FRAME_COUNT = 1000
for frame in range(FRAME_COUNT):
    if frame < FRAME_COUNT / 10:
        im[0, :] = np.random.choice([EMPTY, SAND], im.shape[1])

    for row in range(0 + (frame % 2), im.shape[0] - 1, 2):
        for col in range(0 + (frame % 2), im.shape[1] - 1, 2):
            handle_rect(im, row, col)
            handle_rect(im, row, col + 1)
            handle_square(im, row, col, 1)
            handle_square(im, row, col, -1)
    im[:, 0] = np.full((1, im.shape[0]), EMPTY)
    im[:, -1] = np.full((1, im.shape[0]), EMPTY)
    im[-1, :] = np.full((1, im.shape[1]), EMPTY)
    imgs.append(im.copy())

imgs = [Image.fromarray(a) for a in imgs]
imgs[0].save(fp=fp_out, format='GIF', append_images=imgs,
         save_all=True, duration=1, loop=1)
